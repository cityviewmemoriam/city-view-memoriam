Everything we do at City View Memoriam is done as a tribute to those who have departed this life. The view over Salt Lake Valley is unparalleled & provides countless opportunities for reflection. Here you will feel the sacred peace of knowing you've created a lasting tribute to their beloved memory.

Address: 1001 E 11th Ave, Salt Lake City, UT 84103, USA

Phone: 801-363-7065
